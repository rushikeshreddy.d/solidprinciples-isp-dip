package org.example.InterfaceSegregation;

public interface UPIpayments {

    public void doPayment();

    public void getScratchCard();
}
