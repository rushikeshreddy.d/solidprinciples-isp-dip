package org.example.InterfaceSegregation;

public class GooglePay implements UPIpayments , CashBackManager{

    @Override
    public void doPayment() {

    }

    @Override
    public void getScratchCard() {

    }

    @Override
    public void getCashBackInAmnt() {

    }
}
