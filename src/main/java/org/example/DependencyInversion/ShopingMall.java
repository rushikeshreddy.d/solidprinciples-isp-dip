package org.example.DependencyInversion;

public class ShopingMall  {

    private BankCard bankCard;

    public void doPayment(){
        bankCard.doTransaction();
    }

    public ShopingMall(BankCard bankCard) {
        this.bankCard = bankCard;
    }

    public static void main(String[] args) {
        BankCard bankCard = new CreditCard();
        ShopingMall shopingMall = new ShopingMall(bankCard);
        shopingMall.doPayment();
    }
}
