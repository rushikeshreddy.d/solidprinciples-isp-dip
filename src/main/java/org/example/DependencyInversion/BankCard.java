package org.example.DependencyInversion;

public interface BankCard {

    public void doTransaction();
}
